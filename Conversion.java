
public class Conversion {
	
	public double convertAmount(double amount, String currency) {
		double amt = -1;
		if (currency.contentEquals("CAD")) {
			amt = amount * 1.25;
			return amt;
		}
		else if (currency.equals("USD")) {
			amt = amount * 0.75;
			return amt;
		}
		else {
			System.out.println("Invalid currency type");
			return amt;
		}
	}
}
