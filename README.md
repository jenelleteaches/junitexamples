# Introduction Example for Using JUnit

This repository provides an example of creating an app and then testing it in JUnit.

## Step 1: Create a Currency Conversion App

Create a function converts dollars to either CAD or USD.
1. The function should accept a currency code and amount
2. The function should convert the amount to the currency code (eg: convert $5 to USD)
3. If user enters a currency other than USD or CAD, return -1.

Solution:  ``Conversion.java``

# Step 2: Write the JUnit Tests

Write a JUnit test cases to fully test the app.

Your test cases should exercise all the app's requirements.

Solution: ``ConverionTest.java``

# Step 3: Code Coverage

What is the statement code coverage of your tests?

To automatically calculate the coverage, you can use the EclEmma tool.
The EclEmma tool can be downloaded from the Eclipse Marketplace. Note - the installation may not work on the college computers. You can do it on your own laptop. 