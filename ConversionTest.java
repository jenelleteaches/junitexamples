import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ConversionTest {

	@Test
	void testConvertAmount() {
		
		// Given
		Conversion c = new Conversion();
				
		// When
		double amt = 100.00;
		String unit = "USD";
		double result = c.convertAmount(amt, unit);
		
		// Then
		assertEquals(75d, result);
		assertEquals(125d, c.convertAmount(amt, "CAD"));
		//fail("Not yet implemented");
		
	}

}
